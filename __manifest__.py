# -*- coding: utf-8 -*-
{
    "name": "Accumulative Account Filter",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://gitlab.com/HomebrewSoft.dev",
    "license": "LGPL-3",
    "depends": [
        "account",
        "l10n_mx_edi_landing",
    ],
    "data": [
        # views
        "views/account_move.xml",
        "views/product_template.xml",
        "views/res_partner.xml",
        "views/tax_adjustments_wizard.xml",
        "views/account_tax.xml",
        "views/account_tax_repartition_line.xml",
        "views/account_fiscal_position.xml",
        "views/account_journal.xml",
        "views/product_category.xml",
        "views/account_reconcile_model.xml",
        "views/stock_account_filter.xml",
    ],
}
